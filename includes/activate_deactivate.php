<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/27/2018
 * Time: 10:00 AM
 */

namespace SD\SitemapBuilder;


class activate_deactivate {

	public function activate_plugin() {

		$builder = new builder();

		$result = $builder->update_start_date_option();

		//var_dump( 'start_date' );
		//var_dump( get_option( 'sd_sitemap_start_date' ) );

		if ( $result === false ) {
			//todo abort because it broke
		}

		$builder->update_sitemap_years_and_months_option();

		//var_dump( 'years_and_months' );
		//var_dump( get_option( 'sd_sitemap_years_and_months' ) );

		$builder->update_sitemap_years_option();

		//var_dump( 'years' );
		//var_dump( get_option( 'sd_sitemap_years' ) );

		$builder->update_sitemap_days_option();

		//var_dump( 'days' );
		//var_dump( get_option( 'sd_sitemap_days' ) );

		$dates = get_option( 'sd_sitemap_days', false );
		$years = array_keys( $dates );

		foreach( $years as $year ){
			var_dump( $dates[$year] );
		}


		$result = $builder->update_root_index_file();

		if ( $result !== true ) {
			//todo abort because it broke
		}

		$result = $builder->update_annual_index_files();

		if ( $result !== true ) {
			//todo abort because it broke
		}

		$builder->build_all_sitemap_pages();

		//$days = get_option( 'sd_sitemap_days');

	}

}