<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/28/2018
 * Time: 9:43 PM
 */

namespace SD\SitemapBuilder;


class content extends configuration {

	private $included_post_types = array(
		'post',
		'page',
		'video_of_the_day',
		'citizen_gallery',
		'phakaaathi_log',
		'phakaaathi_video',
		'phakaaathi_gallery',
		'fixtures_results',
		'horse-tips',
		'horse_racing_post',
		'afp_feed_article'
	);

	/**
	 * Build an array [year][months][days] of all dates there a posts for
	 *
	 * @return array $final_dates
	 */
	public function get_years_and_months() {

		$dates       = array();
		$final_dates = array();

		$years = $this->get_years();

		foreach ( $years as $year ) {

			$final_dates[ $year ] = $this->get_months( $year );
		}

		return $final_dates;
	}


	/**
	 * Returns the date published of the first post in the system
	 *
	 * @return bool|false|string
	 */
	public function get_start_date() {

		$args = array();

		$args['post_types']     = $this->get_public_post_types();
		$args['posts_per_page'] = 1;
		$args['fields']         = 'ids';
		$args['orderby']        = 'post_date';
		$args['order']          = 'ASC';

		$post_ids = get_posts( $args );

		if ( is_wp_error( $post_ids ) ) {
			return false;
		}

		/*
		 * Check the years between then and now and add to array
		 */
		$start_date = get_the_date( 'd-m-Y', $post_ids[0] );

		return $start_date;
	}

	/**
	 * Returns an array of years there are posts for using post_date
	 *
	 * @return array
	 */
	public function get_years() {

		$years = array();

		$start_date = get_option( 'sd_sitemap_start_date', false );

		$first_year = date( 'Y', strtotime( $start_date ) );
		$this_year  = date( 'Y', strtotime( 'now' ) );
		$year       = $first_year;

		while ( $year <= $this_year ) {

			$start_date = date( 'Y-m-d', strtotime( $year . '-01-01' ) );
			$end_date   = date( 'Y-m-d', strtotime( $year . '-12-31' ) );

			$posts_exist = $this->get_post_count( $start_date, $end_date );

			if ( $posts_exist === true ) {
				array_push( $years, intval( $year ) );
			}

			$year ++;
		}

		return $years;
	}

	/**
	 * Returns an array of integers, months in the year there are posts for a given year
	 *
	 * @param int $year
	 *
	 * @return array $months
	 */
	public function get_months( $year = 0 ) {

		$months = array();

		for ( $i = 1; $i < 13; $i ++ ) {

			$start_date = date( 'Y-m-d', strtotime( $year . '-' . $i . '-' . 01 ) );

			$d        = cal_days_in_month( CAL_GREGORIAN, $i, $year );
			$end_date = date( 'Y-m-d', strtotime( $year . '-' . $i . '-' . $d ) );

			$posts_exist = $this->get_post_count( $start_date, $end_date );

			if ( $posts_exist === true ) {
				array_push( $months, $i );
			}
		}

		return $months;
	}

	/**
	 * Returns an array of days there are posts for using post_date for a given year and month
	 *
	 * @param int $year
	 * @param int $month
	 *
	 * @return array $days
	 */
	public function get_dates( $year = 0, $month = 0 ) {

		$days = array();

		for ( $i = 1; $i < 32; $i ++ ) {

			if ( checkdate( $month, $i, $year ) !== true ) {
				continue;
			}

			$start_date = \DateTime::createFromFormat( 'Y z H:i', strval( $year ) . ' ' . strval( $i ) . ' 00:01' );
			$end_date   = \DateTime::createFromFormat( 'Y z H:i', strval( $year ) . ' ' . strval( $i ) . ' 23:59' );

			$post_count = $this->get_post_count( $start_date->format( 'Y-m-d H:i' ), $end_date->format( 'Y-m-d H:i' ) );

			if ( $post_count === false ) {
				continue;
			}

			if ( $post_count === true ) {
				array_push( $days, $i );
			}
		}

		return $days;
	}

	/**
	 * Returns and array of dates formatted 'Y-m-d', keyed by year, which contain site-mappable content
	 *
	 * @return array|bool
	 * @throws \Exception
	 */
	public function get_days() {

		$first_date = get_option( 'sd_sitemap_start_date', false );

		if ( $first_date === false ) {
			return false;
		}

		$days        = array();
		$final_dates = array();
		$year        = date( 'Y', strtotime( $first_date ) );

		/*
		 * Build the date range to contain all dates between the first post and the current date
		 */
		$first_date = new \DateTime( $first_date . '00:00' );
		$last_date  = new \DateTime( 'now' );
		$interval   = new \DateInterval( 'P1D' );
		$date_range = new \DatePeriod( $first_date, $interval, $last_date );

		foreach ( $date_range as $date ) {

			/*
			 * We build the output array and reset the content of the $days array
			 * only if the year has changed. Otherwise continue to build the days array.
			 * So the output is year => list of dates for that year.
			 */
			if ( $date->format( 'Y' ) !== $year ) {
				$final_dates[ $year ] = $days;
				$year                 = $date->format( 'Y' );
				$days                 = array();
			} else {
				$year = $date->format( 'Y' );
			}

			$start_date  = $date->format( 'Y-m-d ' . '00:00' );
			$end_date    = $date->format( 'Y-m-d ' . '23:59' );

			$posts_exist = $this->post_exists( $start_date, $end_date );

			if ( $posts_exist === true ) {
				array_push( $days, $date->format( 'Y-m-d' ) );
			}
		}
		return $final_dates;
	}

	/**
	 * Returns the post count for a specific date range. Returns false on error.
	 *
	 * @param string $start_date required
	 * @param string $end_date required
	 *
	 * @return int|false
	 */
	protected function get_post_count( $start_date = '', $end_date = '' ) {

		if ( empty( $start_date ) || empty( $end_date ) ) {
			return false;
		}

		global $wpdb;

		$table_name = $wpdb->prefix . 'posts';

		$count = $wpdb->get_var( $wpdb->prepare(
			"SELECT COUNT(*) FROM $table_name WHERE post_date > %s AND post_date < %s",
			$start_date,
			$end_date )
		);

		if ( is_null( $count ) ) {
			return false;
		}

		if ( $count > 0 ) {
			return true;
		} else {
			return false;
		}


	}

	/**
	 * Returns the true or false on post exists for a specific date range. Returns false on error.
	 *
	 * @param string $start_date required
	 * @param string $end_date required
	 *
	 * @return int|false
	 */
	protected function post_exists( $start_date = '', $end_date = '' ) {

		if ( empty( $start_date ) || empty( $end_date ) ) {
			return false;
		}

		global $wpdb;

		$table_name = $wpdb->prefix . 'posts';

		$post_id = $wpdb->get_var( $wpdb->prepare(
			"SELECT ID FROM $table_name WHERE post_type = 'post' OR post_type = 'page' AND post_status = 'publish' AND post_date > %s AND post_date < %s LIMIT 1",
			$start_date,
			$end_date )
		);

		if ( is_null( $post_id ) ) {
			return false;
		}

		if ( $post_id > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Query and return post_ids for posts in a specific date range.
	 *
	 * @param string $start_date Start date for query
	 * @param string $end_date End date for query
	 * @param int $offset The offset should the number of posts exceed the per sitemap page limit
	 *
	 * @return array $post_ids An array containing the post_ids. Empty on failure.
	 */
	public function get_posts_for_date( $start_date = '', $end_date = '', $offset = 0 ) {

		if ( empty( $start_date ) ) {
			return array();
		}

		if ( empty( $end_date ) ) {
			$end_date = $start_date;
		}

		$args = array();

		$args['post_types']                 = $this->get_public_post_types();
		$args['posts_per_page']             = $this->posts_per_sitemap_page;
		$args['offset']                     = $offset;
		$args['paged']                      = true;
		$args['fields']                     = 'ids';
		$args['date_query'][0]['after']     = $start_date;
		$args['date_query'][0]['before']    = $end_date;
		$args['date_query'][0]['inclusive'] = true;

		$post_ids = get_posts( $args );

		if ( is_wp_error( $post_ids ) ) {
			return array();
		}

		return $post_ids;
	}

	/**
	 * Get correctly formatted date stamp
	 *
	 * @param int $year
	 * @param int $month
	 * @param int $day
	 *
	 * @return false|string $date_stamp Correctly formatted
	 */
	public function get_date_stamp( $year = 0, $month = 0, $day = 0 ) {

		if ( intval( $year ) === 0 || intval( $month ) === 0 || intval( $day ) === 0 ) {
			return false;
		}

		$date       = $year . '-' . $month . '-' . $day;
		$date_stamp = date( 'Y-m-d', strtotime( $date ) );

		return $date_stamp;
	}


	/**
	 * List public post types to add to sitemaps.
	 * Applies the filter 'edit_sitemap_builder_post_types' to the object property $included_post_types
	 *
	 * @void
	 */
	protected function set_public_post_types() {

		$this->included_post_types = apply_filters( 'edit_sitemap_builder_post_types', $this->included_post_types );
	}

	/**
	 * List public post types to add to sitemaps.
	 * Applies the filter 'edit_sitemap_builder_post_types'
	 *
	 * @return array $included_post_types An array of post types to include in the sitemap
	 */
	protected function get_public_post_types() {

		$this->set_public_post_types();
		return $this->included_post_types;
	}

}