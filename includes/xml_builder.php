<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 1/12/2019
 * Time: 11:53 AM
 */

namespace SD\SitemapBuilder;


class xml_builder {

	private static $index_file = '/templates/sitemap_index.xml';
	private static $page_file = '/templates/sitemap_page.xml';
	private $configuration;

	public function __construct() {
		$this->configuration = new configuration();
	}

	public function start_doc( $file_type ) {

		if ( $file_type === 'index' ) {
			$xml = simplexml_load_file( dirname( plugin_dir_path( __FILE__ ) ) . self::$index_file );
		}

		if ( $file_type === 'page' ) {
			$xml = simplexml_load_file( dirname( plugin_dir_path( __FILE__ ) ) . self::$page_file );
		}

		return $xml;
	}

	public function get_page_xml( $post_ids = array() ) {

		if ( empty( $post_ids ) ) {
			return '';
		}

		$xml = $this->start_doc( 'page' );

		foreach ( $post_ids as $post_id ) {

			$permalink          = get_the_permalink( $post_id );
			$last_modified_date = get_the_modified_date( 'Y-m-dTH:mmTZD', $post_id );

			$url = $xml->addChild( 'url' );

			$url->addChild( 'loc', $permalink );
			$url->addChild( 'lastmod', $last_modified_date );
		}

		//todo error handler
		return $xml->asXML();
	}

	public function get_root_index_xml( $years = array() ) {

		if ( empty( $years ) ) {
			return '';
		}

		$xml           = $this->start_doc( 'index' );
		$sitemap_paths = $this->configuration->get_sitemap_paths();

		foreach ( $years as $year ) {

			$link = $sitemap_paths['baseurl'] . $year . '/index.xml';

			$url = $xml->addChild( 'url' );

			$url->addChild( 'loc', $link );
			$url->addChild( 'lastmod', '' );
		}

		//todo error handler
		return $xml->asXML();
	}

	public function get_annual_index_xml( $year, $dates ) {

		if ( empty( $year ) ) {
			return '';
		}

		$xml           = $this->start_doc( 'index' );
		$sitemap_paths = $this->configuration->get_sitemap_paths();

		$path = $sitemap_paths['baseurl'] . $year;

		foreach ( $dates as $date ) {

			$link = $path . '/' . $date . '.xml';

			$url = $xml->addChild( 'url' );

			$url->addChild( 'loc', $link );
			$url->addChild( 'lastmod', '' );
		}

		return $xml->asXML();
	}

}