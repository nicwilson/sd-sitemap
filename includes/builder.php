<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/27/2018
 * Time: 10:02 AM
 */

namespace SD\SitemapBuilder;


class builder {

	protected $configuration;
	protected $writer;

	protected static $instance = null;

	public static function init() {

		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct() {

		$this->configuration = new configuration();
		add_action( 'build_sitemap_for_dates', array( $this, 'build_sitemap_for_dates' ), 10, 1 );
		//$this->writer        = new file_writer();
	}

	public function update_start_date_option() {

		$content_getter = new content();
		$start_date     = $content_getter->get_start_date();

		if ( $start_date === false ) {
			return false;
		}

		$start_date = date( 'Y-m-d', strtotime( $start_date ) );

		$result = update_option( 'sd_sitemap_start_date', $start_date, false );

		return $result;

	}

	/**
	 * Updates the option name 'sd_sitemap_years_and_months' with the an array of years => months
	 * containing site-mappable content
	 */
	public function update_sitemap_years_and_months_option() {

		$content_getter = new content();
		$date_data      = $content_getter->get_years_and_months();

		update_option( 'sd_sitemap_years_and_months', $date_data, false );
	}

	/**
	 * Updates the option name 'sd_sitemap_years' with the an array of years
	 * containing site-mappable content
	 */
	public function update_sitemap_years_option() {

		$date_data = get_option( 'sd_sitemap_years_and_months', false );

		$years = array_keys( $date_data );
		update_option( 'sd_sitemap_years', $years, false );
	}

	/**
	 * Updates the option name sd_sitemap_days with an array of dates containing
	 * site-mappable content, keyed by year
	 *
	 * @throws \Exception
	 */
	public function update_sitemap_days_option() {

		$content_getter = new content();
		$days           = $content_getter->get_days();

		update_option( 'sd_sitemap_days', $days, false );

	}

	/**
	 * Creates root sitemap file. Root sitemap file contains links to all annual index
	 * files, stored in the sitemap directory for the specified year
	 *
	 * @return bool
	 */
	public function update_root_index_file() {

		$years = get_option( 'sd_sitemap_years', false );

		if ( $years === false ) {
			return false;
		}

		$root_sitemap  = ABSPATH . 'sitemap.xml';
		$sitemap_paths = $this->configuration->get_sitemap_paths();

		/*
		 * Ensure the sitemap path exists
		 */
		wp_mkdir_p( $sitemap_paths ['basedir'] );

		/*
		 * Create a directory for each year if it doesn't exist
		 */
		foreach ( $years as $year ) {
			wp_mkdir_p( $sitemap_paths ['basedir'] . $year );
		}

		$sitemap_xml = new xml_builder();
		$xml         = $sitemap_xml->get_root_index_xml( $years );

		$writer = new writer();
		$result = $writer->write_file( $root_sitemap, $xml );

		return $result;
	}

	/**
	 * Creates annual index file, one in each sitemap/year subdirectory. Each file references
	 * the files created for each specific date posts were published on
	 *
	 * @return array|bool
	 *
	 */
	public function update_annual_index_files() {

		$dates_list = get_option( 'sd_sitemap_days', false );
		$results    = array();

		if ( $dates_list === false ) {
			return false;
		}

		$sitemap_xml = new xml_builder();
		$writer      = new writer();
		$sitemap_paths = $this->configuration->get_sitemap_paths();

		foreach ( $dates_list as $year => $dates ) {

			$xml           = $sitemap_xml->get_annual_index_xml( $year, $dates );
			$file_name     = $sitemap_paths['basedir'] . $year . '/index.xml';
			$results[ $year ] = $writer->write_file( $file_name, $xml );
		}

		return $results;
	}

	/**
	 * Sets once-off cron jobs to asynchronously create the sitemap file for
	 * each day posts were published on
	 */
	public function build_all_sitemap_pages() {

		$dates = get_option( 'sd_sitemap_days', false );
		$years = array_keys( $dates );

		$i = 0;
		foreach ( $years as $year ) {

			$timestamp = strtotime( '+' . $i . ' minutes' );

			$split_dates = array_chunk( $dates[ $year ], 100 );

			foreach ( $split_dates as $split_date ) {
				wp_schedule_single_event( $timestamp, 'build_sitemap_for_dates', array( $split_date ) );
			}

			$i =+ 2;
		}
	}

	/**
	 * Generate sitemap for a given date
	 *
	 * @param array $dates
	 *
	 * @void
	 */
	public function build_sitemap_for_dates( $dates ) {

		$writer        = new writer();
		$sitemap_xml   = new xml_builder();
		$content       = new content();
		$offset        = 0;
		$sitemap_paths = $this->configuration->get_sitemap_paths();
		$year          = date( 'Y', strtotime( $dates[0] ) );

		foreach ( $dates as $date ) {

			$post_ids  = $content->get_posts_for_date( $date, $date, $offset );
			$file_name = $sitemap_paths['basedir'] . $year . '/' . $date . '.xml';

			/*
			* If there are no posts make sure there is no sitemap page
			*/
			if ( empty( $post_ids ) ) {

				$writer->delete_file( $file_name );
				continue;
			}

			$xml = $sitemap_xml->get_page_xml( $post_ids );

			$result = $writer->write_file( $file_name, $xml );

			if( $result === false ){
				error_log('SITEMAP ERROR: Writing the sitemap file ' . $file_name . ' failed.' );
			}
			else{
				//error_log('SITEMAP NOTICE: Writing the sitemap file ' . $file_name . ' succeeded.' );
			}
		}
	}
}