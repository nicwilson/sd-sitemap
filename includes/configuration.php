<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/27/2018
 * Time: 9:23 AM
 */

namespace SD\SitemapBuilder;


class configuration {

	private $included_post_types = array(
		'post',
		'page',
		'video_of_the_day',
		'citizen_gallery',
		'phakaaathi_log',
		'phakaaathi_video',
		'phakaaathi_gallery',
		'fixtures_results',
		'horse-tips',
		'horse_racing_post',
		'afp_feed_article'
	);

	/*
	 * Number of entries per sitemap
	 */
	protected $posts_per_sitemap_page = 500;

	/*
	 * Index by year
	 */
	protected $index_by_year = false;

	/*
	 * define( 'MSM_INTERVAL_PER_GENERATION_EVENT', 60 ); // how far apart should full cron generation events be spaced
	 */


	/*
	 * Sitemap storage path and uri details
	 */
	private $sitemap_paths = array(
		'baseurl' => '',
		'basedir' => ''
	);


	protected static $instance = null;


	public static function init() {

		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct() {

		$this->set_sitemap_paths();
	}

	/**
	 * Sets up the sitemap path and directory
	 *
	 * @return array|\WP_Error
	 */
	protected function set_sitemap_paths() {

		$uploads_directory = wp_get_upload_dir();
		$sitemap_directory = $uploads_directory['basedir'] . '/sitemap/';
		$sitemap_url       = $uploads_directory['baseurl'] . '/sitemap/';

		if ( wp_mkdir_p( $sitemap_directory ) ) {

			$sitemap_paths = array(
				'basedir' => $sitemap_directory,
				'baseurl' => $sitemap_url
			);

			$this->sitemap_paths = $sitemap_paths;
		}
	}

	public function get_sitemap_paths() {
		return $this->sitemap_paths;
	}


}