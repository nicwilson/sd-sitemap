<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/27/2018
 * Time: 6:25 PM
 */

namespace SD\SitemapBuilder;


class writer {

	/**
	 * Write a file to the sitemap directory. Returns true on success false on failure
	 *
	 * @param string $file_name
	 * @param string $file_contents
	 *
	 * @return bool
	 */
	public function write_file( $file_name = '', $file_contents = '' ) {

		/*
		 * Here be dragons. We only read/write xml
		 */
		if ( pathinfo( $file_name, PATHINFO_EXTENSION ) !== 'xml' ) {
			return false;
		}

		$credentials = $this->initialize_filesystem();

		if ( is_wp_error( $credentials ) ) {
			return false;
		}

		WP_Filesystem( $credentials );
		global $wp_filesystem;

		$result = $wp_filesystem->put_contents(
			$file_name,
			$file_contents,
			FS_CHMOD_FILE
		);

		return $result;
	}

	/**
	 * Write a file to the sitemap directory. Returns true on success false on failure
	 *
	 * @param string $file_name
	 * @param string $file_contents
	 *
	 * @return bool
	 */
	public function delete_file( $file_name = '' ) {

		/*
		 * Here be dragons. We only read/write xml
		 */
		if ( pathinfo( $file_name, PATHINFO_EXTENSION ) !== 'xml' ) {
			return false;
		}

		$credentials = $this->initialize_filesystem();

		if ( is_wp_error( $credentials ) ) {
			error_log( $credentials->get_error_message() );

			return false;
		}

		WP_Filesystem( $credentials );
		global $wp_filesystem;

		$result = $wp_filesystem->delete( $file_name );

		return $result;
	}


	/**
	 * Loads the WordPress filesystem API, obtains user credentials and returns them. Returns WP Error object on fail.
	 *
	 * @return bool|\WP_Error
	 */
	private function initialize_filesystem() {

		require_once( ABSPATH . 'wp-admin/includes/file.php' );

		$credentials = request_filesystem_credentials( __FILE__, '', false, false, null );

		if ( $credentials === false ) {
			return new \WP_Error(
				'Error',
				'Cannot obtain filesystem credentials' );
		}

		return $credentials;
	}


}