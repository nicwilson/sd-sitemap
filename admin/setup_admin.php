<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/26/2018
 * Time: 12:24 PM
 */

namespace SD\SitemapBuilder;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class setup_admin {



	/**
	 * Register admin menu for sitemap
	 */
	public static function metro_sitemap_menu() {
		$page_hook = add_management_page( __( 'Sitemap', 'metro-sitemaps' ), __( 'Sitemap', 'metro-sitemaps' ), 'manage_options', 'metro-sitemap', array(
			__CLASS__,
			'render_sitemap_options_page'
		) );
		add_action( 'admin_print_scripts-' . $page_hook, array( __CLASS__, 'add_admin_scripts' ) );
	}


	public static function add_admin_scripts() {
		wp_enqueue_script( 'flot', plugins_url( '/js/flot/jquery.flot.js', __FILE__ ), array( 'jquery' ) );
		wp_enqueue_script( 'msm-sitemap-admin', plugins_url( '/js/msm-sitemap-admin.js', __FILE__ ), array(
			'jquery',
			'flot'
		) );
		wp_enqueue_script( 'flot-time', plugins_url( '/js/flot/jquery.flot.time.js', __FILE__ ), array(
			'jquery',
			'flot'
		) );

		wp_enqueue_style( 'msm-sitemap-css', plugins_url( 'css/style.css', __FILE__ ) );
		wp_enqueue_style( 'noticons', '//s0.wordpress.com/i/noticons/noticons.css' );
	}

	public static function ajax_get_sitemap_counts() {
		check_admin_referer( 'msm-sitemap-action' );

		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.', 'metro-sitemaps' ) );
		}

		$n = 10;
		if ( isset( $_REQUEST['num_days'] ) ) {
			$n = intval( $_REQUEST['num_days'] );
		}

		$data = array(
			'total_indexed_urls'   => number_format( Metro_Sitemap::get_total_indexed_url_count() ),
			'total_sitemaps'       => number_format( Metro_Sitemap::count_sitemaps() ),
			'sitemap_indexed_urls' => self::get_recent_sitemap_url_counts( $n ),
		);

		wp_send_json( $data );
	}

	/**
	 * Render admin options page
	 */
	public static function render_sitemap_options_page() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.', 'metro-sitemaps' ) );
		}

		// Array of possible user actions
		$actions = apply_filters( 'msm_sitemap_actions', array() );

		// Start outputting html
		echo '<div class="wrap">';
		screen_icon();
		echo '<h2>' . __( 'Sitemap', 'metro-sitemaps' ) . '</h2>';

		if ( ! self::is_blog_public() ) {
			self::show_action_message( __( 'Oops! Sitemaps are not supported on private blogs. Please make your blog public and try again.', 'metro-sitemaps' ), 'error' );
			echo '</div>';

			return;
		}

		if ( isset( $_POST['action'] ) ) {
			check_admin_referer( 'msm-sitemap-action' );
			foreach ( $actions as $slug => $action ) {
				if ( $action['text'] !== $_POST['action'] ) {
					continue;
				}

				do_action( 'msm_sitemap_action-' . $slug );
				break;
			}
		}

		// All the settings we need to read to display the page
		$sitemap_create_in_progress = get_option( 'msm_sitemap_create_in_progress' ) === true;
		$sitemap_update_last_run    = get_option( 'msm_sitemap_update_last_run' );

		// Determine sitemap status text
		$sitemap_create_status = apply_filters(
			'msm_sitemap_create_status',
			$sitemap_create_in_progress ? __( 'Running', 'metro-sitemaps' ) : __( 'Not Running', 'metro-sitemaps' )
		);

		?>
		<div class="stats-container">
			<div class="stats-box"><strong
					id="sitemap-count"><?php echo number_format( Metro_Sitemap::count_sitemaps() ); ?></strong><?php esc_html_e( 'Sitemaps', 'metro-sitemaps' ); ?>
			</div>
			<div class="stats-box"><strong
					id="sitemap-indexed-url-count"><?php echo number_format( Metro_Sitemap::get_total_indexed_url_count() ); ?></strong><?php esc_html_e( 'Indexed URLs', 'metro-sitemaps' ); ?>
			</div>
			<div class="stats-footer"><span><span
						class="noticon noticon-time"></span><?php esc_html_e( 'Updated', 'metro-sitemaps' ); ?> <strong><?php echo human_time_diff( $sitemap_update_last_run ); ?><?php esc_html_e( 'ago', 'metro-sitemaps' ) ?></strong></span>
			</div>
		</div>

		<h3><?php esc_html_e( 'Latest Sitemaps', 'metro-sitemaps' ); ?></h3>
		<div class="stats-container stats-placeholder"></div>
		<div id="stats-graph-summary"><?php printf( __( 'Max: %s on %s. Showing the last %s days.', 'metro-sitemaps' ), '<span id="stats-graph-max"></span>', '<span id="stats-graph-max-date"></span>', '<span id="stats-graph-num-days"></span>' ); ?></div>

		<h3><?php esc_html_e( 'Generate', 'metro-sitemaps' ); ?></h3>
		<p>
			<strong><?php esc_html_e( 'Sitemap Create Status:', 'metro-sitemaps' ) ?></strong> <?php echo esc_html( $sitemap_create_status ); ?>
		</p>
		<form action="<?php echo menu_page_url( 'metro-sitemap', false ) ?>" method="post" style="float: left;">
			<?php wp_nonce_field( 'msm-sitemap-action' ); ?>
			<?php foreach ( $actions as $action ):
				if ( ! $action['enabled'] ) {
					continue;
				} ?>
				<input type="submit" name="action" class="button-secondary"
				       value="<?php echo esc_attr( $action['text'] ); ?>">
			<?php endforeach; ?>
		</form>
		</div>
		<div id="tooltip"><strong class="content"></strong> <?php esc_html_e( 'indexed urls', 'metro-sitemaps' ); ?>
		</div>
		<?php
	}

	/**
	 * Displays a notice, error or warning to the user
	 *
	 * @param str $message The message to show to the user
	 */
	public static function show_action_message( $message, $level = 'notice' ) {
		$class = 'updated';
		if ( $level === 'warning' ) {
			$class = 'update-nag';
		} elseif ( $level === 'error' ) {
			$class = 'error';
		}

		echo '<div class="' . esc_attr( $class ) . ' msm-sitemap-message"><p>' . wp_kses( $message, wp_kses_allowed_html( 'post' ) ) . '</p></div>';
	}




}